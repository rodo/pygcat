--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: alice; Type: SCHEMA; Schema: -; Owner: rodo
--

CREATE SCHEMA alice;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET search_path = alice, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: flower; Type: TABLE; Schema: alice; Owner: rodo; Tablespace: 
--

CREATE TABLE flower (
    id integer,
    name text,
    properties json
);

SET search_path = public, pg_catalog;

--
-- Name: foo; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE foo (
    id integer,
    name character varying(32),
    ratio double precision,
    indice double precision,
    created_at timestamp with time zone
);

--
-- Name: tools; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE tools (
    id integer,
    name text,
    properties hstore
);


SET search_path = alice, pg_catalog;


CREATE OR REPLACE FUNCTION foo_migration_insert()
    RETURNS TRIGGER AS $BODY$
BEGIN

IF NEW.indice IS NULL THEN
  NEW.indice := NEW.ratio * 2;
END IF;

RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS foo_insert_trigger ON public.foo;

--
--
CREATE TRIGGER car_insert_trigger
    BEFORE INSERT ON public.foo
    FOR EACH ROW
    EXECUTE PROCEDURE foo_migration_insert();

--
-- Data for Name: flower; Type: TABLE DATA; Schema: alice; Owner: rodo
--

COPY flower (id, name, properties) FROM stdin;
1	rose	{"color":"red"}
\.


SET search_path = public, pg_catalog;

--
-- Data for Name: foo; Type: TABLE DATA; Schema: public; Owner: rodo
--

COPY foo (id, name, ratio, created_at) FROM stdin;
1	alpha	\N	\N
2	alpha	\N	\N
3	alpha	\N	\N
4	alpha	\N	\N
5	alpha	\N	\N
6	alpha	\N	\N
7	alpha	\N	\N
8	alpha	\N	\N
9	alpha	\N	\N
10	alpha	\N	\N
11	alpha	\N	\N
12	alpha	\N	\N
13	alpha	\N	\N
14	alpha	\N	\N
15	alpha	\N	\N
16	alpha	\N	\N
17	alpha	\N	\N
18	alpha	\N	\N
19	alpha	\N	\N
20	alpha	\N	\N
21	alpha	\N	\N
22	alpha	\N	\N
23	alpha	\N	\N
24	alpha	\N	\N
25	alpha	\N	\N
26	alpha	\N	\N
27	alpha	\N	\N
28	alpha	\N	\N
29	alpha	\N	\N
30	alpha	\N	\N
31	alpha	\N	\N
32	alpha	\N	\N
33	alpha	\N	\N
34	alpha	\N	\N
35	alpha	\N	\N
36	alpha	\N	\N
37	alpha	\N	\N
38	alpha	\N	\N
39	alpha	\N	\N
40	alpha	\N	\N
41	alpha	\N	\N
42	alpha	\N	\N
43	alpha	\N	\N
44	alpha	\N	\N
45	alpha	\N	\N
46	alpha	\N	\N
47	alpha	\N	\N
48	alpha	\N	\N
49	alpha	\N	\N
50	alpha	\N	\N
51	alpha	\N	\N
52	alpha	\N	\N
53	alpha	\N	\N
54	alpha	\N	\N
55	alpha	\N	\N
56	alpha	\N	\N
57	alpha	\N	\N
58	alpha	\N	\N
59	alpha	\N	\N
60	alpha	\N	\N
61	alpha	\N	\N
62	alpha	\N	\N
63	alpha	\N	\N
64	alpha	\N	\N
65	alpha	\N	\N
66	alpha	\N	\N
67	alpha	\N	\N
68	alpha	\N	\N
69	alpha	\N	\N
70	alpha	\N	\N
71	alpha	\N	\N
72	alpha	\N	\N
73	alpha	\N	\N
74	alpha	\N	\N
75	alpha	\N	\N
76	alpha	\N	\N
77	alpha	\N	\N
78	alpha	\N	\N
79	alpha	\N	\N
80	alpha	\N	\N
81	alpha	\N	\N
82	alpha	\N	\N
83	alpha	\N	\N
84	alpha	\N	\N
85	alpha	\N	\N
86	alpha	\N	\N
87	alpha	\N	\N
88	alpha	\N	\N
89	alpha	\N	\N
90	alpha	\N	\N
91	alpha	\N	\N
92	alpha	\N	\N
93	alpha	\N	\N
94	alpha	\N	\N
95	alpha	\N	\N
96	alpha	\N	\N
97	alpha	\N	\N
98	alpha	\N	\N
99	alpha	\N	\N
100	alpha	\N	\N
101	alpha	\N	\N
102	alpha	\N	\N
103	alpha	\N	\N
104	alpha	\N	\N
105	alpha	\N	\N
106	alpha	\N	\N
107	alpha	\N	\N
108	alpha	\N	\N
109	alpha	\N	\N
110	alpha	\N	\N
111	alpha	\N	\N
112	alpha	\N	\N
113	alpha	\N	\N
114	alpha	\N	\N
115	alpha	\N	\N
116	alpha	\N	\N
117	alpha	\N	\N
118	alpha	\N	\N
119	alpha	\N	\N
120	alpha	\N	\N
121	alpha	\N	\N
122	alpha	\N	\N
123	alpha	\N	\N
124	alpha	\N	\N
125	alpha	\N	\N
126	alpha	\N	\N
127	alpha	\N	\N
128	alpha	\N	\N
129	alpha	\N	\N
130	alpha	\N	\N
131	alpha	\N	\N
132	alpha	\N	\N
133	alpha	\N	\N
134	alpha	\N	\N
135	alpha	\N	\N
136	alpha	\N	\N
137	alpha	\N	\N
138	alpha	\N	\N
139	alpha	\N	\N
140	alpha	\N	\N
141	alpha	\N	\N
142	alpha	\N	\N
143	alpha	\N	\N
144	alpha	\N	\N
145	alpha	\N	\N
146	alpha	\N	\N
147	alpha	\N	\N
148	alpha	\N	\N
149	alpha	\N	\N
150	alpha	\N	\N
151	alpha	\N	\N
152	alpha	\N	\N
153	alpha	\N	\N
154	alpha	\N	\N
155	alpha	\N	\N
156	alpha	\N	\N
157	alpha	\N	\N
158	alpha	\N	\N
159	alpha	\N	\N
160	alpha	\N	\N
161	alpha	\N	\N
162	alpha	\N	\N
163	alpha	\N	\N
164	alpha	\N	\N
165	alpha	\N	\N
166	alpha	\N	\N
167	alpha	\N	\N
168	alpha	\N	\N
169	alpha	\N	\N
170	alpha	\N	\N
171	alpha	\N	\N
172	alpha	\N	\N
173	alpha	\N	\N
174	alpha	\N	\N
175	alpha	\N	\N
176	alpha	\N	\N
177	alpha	\N	\N
178	alpha	\N	\N
179	alpha	\N	\N
180	alpha	\N	\N
181	alpha	\N	\N
182	alpha	\N	\N
183	alpha	\N	\N
184	alpha	\N	\N
185	alpha	\N	\N
186	alpha	\N	\N
187	alpha	\N	\N
188	alpha	\N	\N
189	alpha	\N	\N
190	alpha	\N	\N
191	alpha	\N	\N
192	alpha	\N	\N
193	alpha	\N	\N
194	alpha	\N	\N
195	alpha	\N	\N
196	alpha	\N	\N
197	alpha	\N	\N
198	alpha	\N	\N
199	alpha	\N	\N
200	alpha	\N	\N
201	alpha	\N	\N
202	alpha	\N	\N
203	alpha	\N	\N
204	alpha	\N	\N
205	alpha	\N	\N
206	alpha	\N	\N
207	alpha	\N	\N
208	alpha	\N	\N
209	alpha	\N	\N
210	alpha	\N	\N
211	alpha	\N	\N
212	alpha	\N	\N
213	alpha	\N	\N
214	alpha	\N	\N
215	alpha	\N	\N
216	alpha	\N	\N
217	alpha	\N	\N
218	alpha	\N	\N
219	alpha	\N	\N
220	alpha	\N	\N
221	alpha	\N	\N
222	alpha	\N	\N
223	alpha	\N	\N
224	alpha	\N	\N
225	alpha	\N	\N
226	alpha	\N	\N
227	alpha	\N	\N
228	alpha	\N	\N
229	alpha	\N	\N
230	alpha	\N	\N
231	alpha	\N	\N
232	alpha	\N	\N
233	alpha	\N	\N
234	alpha	\N	\N
235	alpha	\N	\N
236	alpha	\N	\N
237	alpha	\N	\N
238	alpha	\N	\N
239	alpha	\N	\N
240	alpha	\N	\N
241	alpha	\N	\N
242	alpha	\N	\N
243	alpha	\N	\N
244	alpha	\N	\N
245	alpha	\N	\N
246	alpha	\N	\N
247	alpha	\N	\N
248	alpha	\N	\N
249	alpha	\N	\N
250	alpha	\N	\N
251	alpha	\N	\N
252	alpha	\N	\N
253	alpha	\N	\N
254	alpha	\N	\N
255	alpha	\N	\N
256	alpha	\N	\N
257	alpha	\N	\N
258	alpha	\N	\N
259	alpha	\N	\N
260	alpha	\N	\N
261	alpha	\N	\N
262	alpha	\N	\N
263	alpha	\N	\N
264	alpha	\N	\N
265	alpha	\N	\N
266	alpha	\N	\N
267	alpha	\N	\N
268	alpha	\N	\N
269	alpha	\N	\N
270	alpha	\N	\N
271	alpha	\N	\N
272	alpha	\N	\N
273	alpha	\N	\N
274	alpha	\N	\N
275	alpha	\N	\N
276	alpha	\N	\N
277	alpha	\N	\N
278	alpha	\N	\N
279	alpha	\N	\N
280	alpha	\N	\N
281	alpha	\N	\N
282	alpha	\N	\N
283	alpha	\N	\N
284	alpha	\N	\N
285	alpha	\N	\N
286	alpha	\N	\N
287	alpha	\N	\N
288	alpha	\N	\N
289	alpha	\N	\N
290	alpha	\N	\N
291	alpha	\N	\N
292	alpha	\N	\N
293	alpha	\N	\N
294	alpha	\N	\N
295	alpha	\N	\N
296	alpha	\N	\N
297	alpha	\N	\N
298	alpha	\N	\N
299	alpha	\N	\N
300	alpha	\N	\N
301	alpha	\N	\N
302	alpha	\N	\N
303	alpha	\N	\N
304	alpha	\N	\N
305	alpha	\N	\N
306	alpha	\N	\N
307	alpha	\N	\N
308	alpha	\N	\N
309	alpha	\N	\N
310	alpha	\N	\N
311	alpha	\N	\N
312	alpha	\N	\N
313	alpha	\N	\N
314	alpha	\N	\N
315	alpha	\N	\N
316	alpha	\N	\N
317	alpha	\N	\N
318	alpha	\N	\N
319	alpha	\N	\N
320	alpha	\N	\N
321	alpha	\N	\N
322	alpha	\N	\N
323	alpha	\N	\N
324	alpha	\N	\N
325	alpha	\N	\N
326	alpha	\N	\N
327	alpha	\N	\N
328	alpha	\N	\N
329	alpha	\N	\N
330	alpha	\N	\N
331	alpha	\N	\N
332	alpha	\N	\N
333	alpha	\N	\N
334	alpha	\N	\N
335	alpha	\N	\N
336	alpha	\N	\N
337	alpha	\N	\N
338	alpha	\N	\N
339	alpha	\N	\N
340	alpha	\N	\N
341	alpha	\N	\N
342	alpha	\N	\N
343	alpha	\N	\N
344	alpha	\N	\N
345	alpha	\N	\N
346	alpha	\N	\N
347	alpha	\N	\N
348	alpha	\N	\N
349	alpha	\N	\N
350	alpha	\N	\N
351	alpha	\N	\N
352	alpha	\N	\N
353	alpha	\N	\N
354	alpha	\N	\N
355	alpha	\N	\N
356	alpha	\N	\N
357	alpha	\N	\N
358	alpha	\N	\N
359	alpha	\N	\N
360	alpha	\N	\N
361	alpha	\N	\N
362	alpha	\N	\N
363	alpha	\N	\N
364	alpha	\N	\N
365	alpha	\N	\N
366	alpha	\N	\N
367	alpha	\N	\N
368	alpha	\N	\N
369	alpha	\N	\N
370	alpha	\N	\N
371	alpha	\N	\N
372	alpha	\N	\N
373	alpha	\N	\N
374	alpha	\N	\N
375	alpha	\N	\N
376	alpha	\N	\N
377	alpha	\N	\N
378	alpha	\N	\N
379	alpha	\N	\N
380	alpha	\N	\N
381	alpha	\N	\N
382	alpha	\N	\N
383	alpha	\N	\N
384	alpha	\N	\N
385	alpha	\N	\N
386	alpha	\N	\N
387	alpha	\N	\N
388	alpha	\N	\N
389	alpha	\N	\N
390	alpha	\N	\N
391	alpha	\N	\N
392	alpha	\N	\N
393	alpha	\N	\N
394	alpha	\N	\N
395	alpha	\N	\N
396	alpha	\N	\N
397	alpha	\N	\N
398	alpha	\N	\N
399	alpha	\N	\N
400	alpha	\N	\N
401	alpha	\N	\N
402	alpha	\N	\N
403	alpha	\N	\N
404	alpha	\N	\N
405	alpha	\N	\N
406	alpha	\N	\N
407	alpha	\N	\N
408	alpha	\N	\N
409	alpha	\N	\N
410	alpha	\N	\N
411	alpha	\N	\N
412	alpha	\N	\N
413	alpha	\N	\N
414	alpha	\N	\N
415	alpha	\N	\N
416	alpha	\N	\N
417	alpha	\N	\N
418	alpha	\N	\N
419	alpha	\N	\N
420	alpha	\N	\N
421	alpha	\N	\N
422	alpha	\N	\N
423	alpha	\N	\N
424	alpha	\N	\N
425	alpha	\N	\N
426	alpha	\N	\N
427	alpha	\N	\N
428	alpha	\N	\N
429	alpha	\N	\N
430	alpha	\N	\N
431	alpha	\N	\N
432	alpha	\N	\N
433	alpha	\N	\N
434	alpha	\N	\N
435	alpha	\N	\N
436	alpha	\N	\N
437	alpha	\N	\N
438	alpha	\N	\N
439	alpha	\N	\N
440	alpha	\N	\N
441	alpha	\N	\N
442	alpha	\N	\N
443	alpha	\N	\N
444	alpha	\N	\N
445	alpha	\N	\N
446	alpha	\N	\N
447	alpha	\N	\N
448	alpha	\N	\N
449	alpha	\N	\N
450	alpha	\N	\N
451	alpha	\N	\N
452	alpha	\N	\N
453	alpha	\N	\N
454	alpha	\N	\N
455	alpha	\N	\N
456	alpha	\N	\N
457	alpha	\N	\N
458	alpha	\N	\N
459	alpha	\N	\N
460	alpha	\N	\N
461	alpha	\N	\N
462	alpha	\N	\N
463	alpha	\N	\N
464	alpha	\N	\N
465	alpha	\N	\N
466	alpha	\N	\N
467	alpha	\N	\N
468	alpha	\N	\N
469	alpha	\N	\N
470	alpha	\N	\N
471	alpha	\N	\N
472	alpha	\N	\N
473	alpha	\N	\N
474	alpha	\N	\N
475	alpha	\N	\N
476	alpha	\N	\N
477	alpha	\N	\N
478	alpha	\N	\N
479	alpha	\N	\N
480	alpha	\N	\N
481	alpha	\N	\N
482	alpha	\N	\N
483	alpha	\N	\N
484	alpha	\N	\N
485	alpha	\N	\N
486	alpha	\N	\N
487	alpha	\N	\N
488	alpha	\N	\N
489	alpha	\N	\N
490	alpha	\N	\N
491	alpha	\N	\N
492	alpha	\N	\N
493	alpha	\N	\N
494	alpha	\N	\N
495	alpha	\N	\N
496	alpha	\N	\N
497	alpha	\N	\N
498	alpha	\N	\N
499	alpha	\N	\N
500	alpha	\N	\N
501	alpha	\N	\N
502	alpha	\N	\N
503	alpha	\N	\N
504	alpha	\N	\N
505	alpha	\N	\N
506	alpha	\N	\N
507	alpha	\N	\N
508	alpha	\N	\N
509	alpha	\N	\N
510	alpha	\N	\N
511	alpha	\N	\N
512	alpha	\N	\N
513	alpha	\N	\N
514	alpha	\N	\N
515	alpha	\N	\N
516	alpha	\N	\N
517	alpha	\N	\N
518	alpha	\N	\N
519	alpha	\N	\N
520	alpha	\N	\N
521	alpha	\N	\N
522	alpha	\N	\N
523	alpha	\N	\N
524	alpha	\N	\N
525	alpha	\N	\N
526	alpha	\N	\N
527	alpha	\N	\N
528	alpha	\N	\N
529	alpha	\N	\N
530	alpha	\N	\N
531	alpha	\N	\N
532	alpha	\N	\N
533	alpha	\N	\N
534	alpha	\N	\N
535	alpha	\N	\N
536	alpha	\N	\N
537	alpha	\N	\N
538	alpha	\N	\N
539	alpha	\N	\N
540	alpha	\N	\N
541	alpha	\N	\N
542	alpha	\N	\N
543	alpha	\N	\N
544	alpha	\N	\N
545	alpha	\N	\N
546	alpha	\N	\N
547	alpha	\N	\N
548	alpha	\N	\N
549	alpha	\N	\N
550	alpha	\N	\N
551	alpha	\N	\N
552	alpha	\N	\N
553	alpha	\N	\N
554	alpha	\N	\N
555	alpha	\N	\N
556	alpha	\N	\N
557	alpha	\N	\N
558	alpha	\N	\N
559	alpha	\N	\N
560	alpha	\N	\N
561	alpha	\N	\N
562	alpha	\N	\N
563	alpha	\N	\N
564	alpha	\N	\N
565	alpha	\N	\N
566	alpha	\N	\N
567	alpha	\N	\N
568	alpha	\N	\N
569	alpha	\N	\N
570	alpha	\N	\N
571	alpha	\N	\N
572	alpha	\N	\N
573	alpha	\N	\N
574	alpha	\N	\N
575	alpha	\N	\N
576	alpha	\N	\N
577	alpha	\N	\N
578	alpha	\N	\N
579	alpha	\N	\N
580	alpha	\N	\N
581	alpha	\N	\N
582	alpha	\N	\N
583	alpha	\N	\N
584	alpha	\N	\N
585	alpha	\N	\N
586	alpha	\N	\N
587	alpha	\N	\N
588	alpha	\N	\N
589	alpha	\N	\N
590	alpha	\N	\N
591	alpha	\N	\N
592	alpha	\N	\N
593	alpha	\N	\N
594	alpha	\N	\N
595	alpha	\N	\N
596	alpha	\N	\N
597	alpha	\N	\N
598	alpha	\N	\N
599	alpha	\N	\N
600	alpha	\N	\N
601	alpha	\N	\N
602	alpha	\N	\N
603	alpha	\N	\N
604	alpha	\N	\N
605	alpha	\N	\N
606	alpha	\N	\N
607	alpha	\N	\N
608	alpha	\N	\N
609	alpha	\N	\N
610	alpha	\N	\N
611	alpha	\N	\N
612	alpha	\N	\N
613	alpha	\N	\N
614	alpha	\N	\N
615	alpha	\N	\N
616	alpha	\N	\N
617	alpha	\N	\N
618	alpha	\N	\N
619	alpha	\N	\N
620	alpha	\N	\N
621	alpha	\N	\N
622	alpha	\N	\N
623	alpha	\N	\N
624	alpha	\N	\N
625	alpha	\N	\N
626	alpha	\N	\N
627	alpha	\N	\N
628	alpha	\N	\N
629	alpha	\N	\N
630	alpha	\N	\N
631	alpha	\N	\N
632	alpha	\N	\N
633	alpha	\N	\N
634	alpha	\N	\N
635	alpha	\N	\N
636	alpha	\N	\N
637	alpha	\N	\N
638	alpha	\N	\N
639	alpha	\N	\N
640	alpha	\N	\N
641	alpha	\N	\N
642	alpha	\N	\N
643	alpha	\N	\N
644	alpha	\N	\N
645	alpha	\N	\N
646	alpha	\N	\N
647	alpha	\N	\N
648	alpha	\N	\N
649	alpha	\N	\N
650	alpha	\N	\N
651	alpha	\N	\N
652	alpha	\N	\N
653	alpha	\N	\N
654	alpha	\N	\N
655	alpha	\N	\N
656	alpha	\N	\N
657	alpha	\N	\N
658	alpha	\N	\N
659	alpha	\N	\N
660	alpha	\N	\N
661	alpha	\N	\N
662	alpha	\N	\N
663	alpha	\N	\N
664	alpha	\N	\N
665	alpha	\N	\N
666	alpha	\N	\N
667	alpha	\N	\N
668	alpha	\N	\N
669	alpha	\N	\N
670	alpha	\N	\N
671	alpha	\N	\N
672	alpha	\N	\N
673	alpha	\N	\N
674	alpha	\N	\N
675	alpha	\N	\N
676	alpha	\N	\N
677	alpha	\N	\N
678	alpha	\N	\N
679	alpha	\N	\N
680	alpha	\N	\N
681	alpha	\N	\N
682	alpha	\N	\N
683	alpha	\N	\N
684	alpha	\N	\N
685	alpha	\N	\N
686	alpha	\N	\N
687	alpha	\N	\N
688	alpha	\N	\N
689	alpha	\N	\N
690	alpha	\N	\N
691	alpha	\N	\N
692	alpha	\N	\N
693	alpha	\N	\N
694	alpha	\N	\N
695	alpha	\N	\N
696	alpha	\N	\N
697	alpha	\N	\N
698	alpha	\N	\N
699	alpha	\N	\N
700	alpha	\N	\N
701	alpha	\N	\N
702	alpha	\N	\N
703	alpha	\N	\N
704	alpha	\N	\N
705	alpha	\N	\N
706	alpha	\N	\N
707	alpha	\N	\N
708	alpha	\N	\N
709	alpha	\N	\N
710	alpha	\N	\N
711	alpha	\N	\N
712	alpha	\N	\N
713	alpha	\N	\N
714	alpha	\N	\N
715	alpha	\N	\N
716	alpha	\N	\N
717	alpha	\N	\N
718	alpha	\N	\N
719	alpha	\N	\N
720	alpha	\N	\N
721	alpha	\N	\N
722	alpha	\N	\N
723	alpha	\N	\N
724	alpha	\N	\N
725	alpha	\N	\N
726	alpha	\N	\N
727	alpha	\N	\N
728	alpha	\N	\N
729	alpha	\N	\N
730	alpha	\N	\N
731	alpha	\N	\N
732	alpha	\N	\N
733	alpha	\N	\N
734	alpha	\N	\N
735	alpha	\N	\N
736	alpha	\N	\N
737	alpha	\N	\N
738	alpha	\N	\N
739	alpha	\N	\N
740	alpha	\N	\N
741	alpha	\N	\N
742	alpha	\N	\N
743	alpha	\N	\N
744	alpha	\N	\N
745	alpha	\N	\N
746	alpha	\N	\N
747	alpha	\N	\N
748	alpha	\N	\N
749	alpha	\N	\N
750	alpha	\N	\N
751	alpha	\N	\N
752	alpha	\N	\N
753	alpha	\N	\N
754	alpha	\N	\N
755	alpha	\N	\N
756	alpha	\N	\N
757	alpha	\N	\N
758	alpha	\N	\N
759	alpha	\N	\N
760	alpha	\N	\N
761	alpha	\N	\N
762	alpha	\N	\N
763	alpha	\N	\N
764	alpha	\N	\N
765	alpha	\N	\N
766	alpha	\N	\N
767	alpha	\N	\N
768	alpha	\N	\N
769	alpha	\N	\N
770	alpha	\N	\N
771	alpha	\N	\N
772	alpha	\N	\N
773	alpha	\N	\N
774	alpha	\N	\N
775	alpha	\N	\N
776	alpha	\N	\N
777	alpha	\N	\N
778	alpha	\N	\N
779	alpha	\N	\N
780	alpha	\N	\N
781	alpha	\N	\N
782	alpha	\N	\N
783	alpha	\N	\N
784	alpha	\N	\N
785	alpha	\N	\N
786	alpha	\N	\N
787	alpha	\N	\N
788	alpha	\N	\N
789	alpha	\N	\N
790	alpha	\N	\N
791	alpha	\N	\N
792	alpha	\N	\N
793	alpha	\N	\N
794	alpha	\N	\N
795	alpha	\N	\N
796	alpha	\N	\N
797	alpha	\N	\N
798	alpha	\N	\N
799	alpha	\N	\N
800	alpha	\N	\N
801	alpha	\N	\N
802	alpha	\N	\N
803	alpha	\N	\N
804	alpha	\N	\N
805	alpha	\N	\N
806	alpha	\N	\N
807	alpha	\N	\N
808	alpha	\N	\N
809	alpha	\N	\N
810	alpha	\N	\N
811	alpha	\N	\N
812	alpha	\N	\N
813	alpha	\N	\N
814	alpha	\N	\N
815	alpha	\N	\N
816	alpha	\N	\N
817	alpha	\N	\N
818	alpha	\N	\N
819	alpha	\N	\N
820	alpha	\N	\N
821	alpha	\N	\N
822	alpha	\N	\N
823	alpha	\N	\N
824	alpha	\N	\N
825	alpha	\N	\N
826	alpha	\N	\N
827	alpha	\N	\N
828	alpha	\N	\N
829	alpha	\N	\N
830	alpha	\N	\N
831	alpha	\N	\N
832	alpha	\N	\N
833	alpha	\N	\N
834	alpha	\N	\N
835	alpha	\N	\N
836	alpha	\N	\N
837	alpha	\N	\N
838	alpha	\N	\N
839	alpha	\N	\N
840	alpha	\N	\N
841	alpha	\N	\N
842	alpha	\N	\N
843	alpha	\N	\N
844	alpha	\N	\N
845	alpha	\N	\N
846	alpha	\N	\N
847	alpha	\N	\N
848	alpha	\N	\N
849	alpha	\N	\N
850	alpha	\N	\N
851	alpha	\N	\N
852	alpha	\N	\N
853	alpha	\N	\N
854	alpha	\N	\N
855	alpha	\N	\N
856	alpha	\N	\N
857	alpha	\N	\N
858	alpha	\N	\N
859	alpha	\N	\N
860	alpha	\N	\N
861	alpha	\N	\N
862	alpha	\N	\N
863	alpha	\N	\N
864	alpha	\N	\N
865	alpha	\N	\N
866	alpha	\N	\N
867	alpha	\N	\N
868	alpha	\N	\N
869	alpha	\N	\N
870	alpha	\N	\N
871	alpha	\N	\N
872	alpha	\N	\N
873	alpha	\N	\N
874	alpha	\N	\N
875	alpha	\N	\N
876	alpha	\N	\N
877	alpha	\N	\N
878	alpha	\N	\N
879	alpha	\N	\N
880	alpha	\N	\N
881	alpha	\N	\N
882	alpha	\N	\N
883	alpha	\N	\N
884	alpha	\N	\N
885	alpha	\N	\N
886	alpha	\N	\N
887	alpha	\N	\N
888	alpha	\N	\N
889	alpha	\N	\N
890	alpha	\N	\N
891	alpha	\N	\N
892	alpha	\N	\N
893	alpha	\N	\N
894	alpha	\N	\N
895	alpha	\N	\N
896	alpha	\N	\N
897	alpha	\N	\N
898	alpha	\N	\N
899	alpha	\N	\N
900	alpha	\N	\N
901	alpha	\N	\N
902	alpha	\N	\N
903	alpha	\N	\N
904	alpha	\N	\N
905	alpha	\N	\N
906	alpha	\N	\N
907	alpha	\N	\N
908	alpha	\N	\N
909	alpha	\N	\N
910	alpha	\N	\N
911	alpha	\N	\N
912	alpha	\N	\N
913	alpha	\N	\N
914	alpha	\N	\N
915	alpha	\N	\N
916	alpha	\N	\N
917	alpha	\N	\N
918	alpha	\N	\N
919	alpha	\N	\N
920	alpha	\N	\N
921	alpha	\N	\N
922	alpha	\N	\N
923	alpha	\N	\N
924	alpha	\N	\N
925	alpha	\N	\N
926	alpha	\N	\N
927	alpha	\N	\N
928	alpha	\N	\N
929	alpha	\N	\N
930	alpha	\N	\N
931	alpha	\N	\N
932	alpha	\N	\N
933	alpha	\N	\N
934	alpha	\N	\N
935	alpha	\N	\N
936	alpha	\N	\N
937	alpha	\N	\N
938	alpha	\N	\N
939	alpha	\N	\N
940	alpha	\N	\N
941	alpha	\N	\N
942	alpha	\N	\N
943	alpha	\N	\N
944	alpha	\N	\N
945	alpha	\N	\N
946	alpha	\N	\N
947	alpha	\N	\N
948	alpha	\N	\N
949	alpha	\N	\N
950	alpha	\N	\N
951	alpha	\N	\N
952	alpha	\N	\N
953	alpha	\N	\N
954	alpha	\N	\N
955	alpha	\N	\N
956	alpha	\N	\N
957	alpha	\N	\N
958	alpha	\N	\N
959	alpha	\N	\N
960	alpha	\N	\N
961	alpha	\N	\N
962	alpha	\N	\N
963	alpha	\N	\N
964	alpha	\N	\N
965	alpha	\N	\N
966	alpha	\N	\N
967	alpha	\N	\N
968	alpha	\N	\N
969	alpha	\N	\N
970	alpha	\N	\N
971	alpha	\N	\N
972	alpha	\N	\N
973	alpha	\N	\N
974	alpha	\N	\N
975	alpha	\N	\N
976	alpha	\N	\N
977	alpha	\N	\N
978	alpha	\N	\N
979	alpha	\N	\N
980	alpha	\N	\N
981	alpha	\N	\N
982	alpha	\N	\N
983	alpha	\N	\N
984	alpha	\N	\N
985	alpha	\N	\N
986	alpha	\N	\N
987	alpha	\N	\N
988	alpha	\N	\N
989	alpha	\N	\N
990	alpha	\N	\N
991	alpha	\N	\N
992	alpha	\N	\N
993	alpha	\N	\N
994	alpha	\N	\N
995	alpha	\N	\N
996	alpha	\N	\N
997	alpha	\N	\N
998	alpha	\N	\N
999	alpha	\N	\N
1000	alpha	\N	\N
\.


--
-- Data for Name: tools; Type: TABLE DATA; Schema: public; Owner: rodo
--

COPY tools (id, name, properties) FROM stdin;
\.


--
-- Name: foo_name_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX foo_name_idx ON foo USING btree (name);


--
-- Name: foo_name_ratio_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX foo_name_ratio_idx ON foo USING btree (name, ratio);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

