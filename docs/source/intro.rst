============
Introduction
============

PygCatalog simply access to schema informations, you can easily know if
a column is present in an index or use of special type in a table.

The only one requirements is to have a psycopg2 connection open to a
database, PygCatalog will explore all schemas
