.. pygcat documentation master file, created by
   sphinx-quickstart on Mon Sep 28 17:00:46 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PygCat
======

Make access to postgresql schema informations easy, is the column **C** is
present in an index, is the table **T** contains a primary key, PygCat
can answers this.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   pygcat
   otherclass

Contributing
============

* `Source code <https://gitlab.com/rodo/pygcat>`_

* `Issues <https://gitlab.com/rodo/pygcat/issues>`_  
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

