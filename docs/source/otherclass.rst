==========
Exceptions
==========
      
.. autoclass:: pygcat.SchemaDoesNotExists
   :members:

.. autoclass:: pygcat.TableDoesNotExists
   :members:

.. autoclass:: pygcat.ColumnDoesNotExists
   :members:   
      
