#!/usr/bin/env python
"""

Usage : python table_rst.py DIRECTORY VERSION_NUMBER

Requirements : tabulate, psycopg2

"""
import sys
from os import path
import psycopg2
from pygcat import PygCatalog
from tabulate import tabulate

# open a connection to PostgreSQL database
# connection settings are set via libpq ENV VARS
conn = psycopg2.connect('')

# instantiate PygCatalog
pyg = PygCatalog(conn)

# fetch all available table in 'public' schema
tables = pyg.get_tables()

def defaultval(value):
    if value is None:
        return ''
    return value

def yesno(value):
    if value == 1:
        return 'yes'
    else:
        return ''

fmt = "%s | %s | %s | %s\n"

headers = ['name', 'type', 'nullable', 'default']

for table in tables:
    cols = pyg.describe_table(table)

    datas = []

    for col in cols:
        data = [col.get('name'),
                col.get('type'),
                col.get('is_nullable'),
                defaultval(col.get('default_value'))]

        datas.append(data)

    fname = path.join(sys.argv[1], "%s.rst" % (table))
    f = open(fname,'w')
    sys.stdout.write("Open %s\n" % (fname))

    f.write("%s\n%s\n" % (len(table) * '=', table))
    f.write("%s\n\n" % (len(table) * '='))

    f.write("version **%s**\n\n" % (sys.argv[2]))

    f.write(tabulate(datas, headers, tablefmt="rst"))

    f.write("\n\nIndexes\n")
    f.write("%s\n\n" % (len(table) * '-'))

    # fetch all indexes
    cols = pyg.get_indexes(schema='public', table=table)
    datas = []

    for key in cols.keys():
        index = cols[key]
        datas.append([key,
                      ",".join(index['columns']),
                      yesno(index['is_unique']),
                      index['access_method']
                      ])

    f.write(tabulate(datas, ['name', 'columns', 'unique', 'am'], tablefmt="rst"))

    f.close()
