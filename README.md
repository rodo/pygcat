* [![build status](https://gitlab.com/ci/projects/9171/status.png?ref=master)](https://gitlab.com/ci/projects/9171?ref=master)

* [![Documentation Status](https://readthedocs.org/projects/pygcat/badge/?version=latest)](http://pygcat.readthedocs.org/en/latest/?badge=latest)

Introduction
============

Read the [PostgreSQL system
catalog](http://www.postgresql.org/docs/current/static/catalogs.html)
directly with a python class. Permits to find information like number
of index on a table, or if a knwon column is indexed or not.

Samples usage
=============

Look for table's size present in the database

```
import psycopg2

# open a connection to PostgreSQL
conn = psycopg2.connect('')

#
from pygcat import PygCatalog
pyg = PygCatalog(conn)
        
tables = pyg.get_tables()

for key in tables:
    print "there is %s rows in %s\n" % (tables[key][tuple], key)

```

Is a column indexed ?

```
>>> pyg = PygCatalog(conn)
>>> pyg.is_column_indexed("name", "table_clients")
true
```
