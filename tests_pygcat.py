# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""

"""
from unittest import TestCase
import psycopg2
from pygcat import PygCatalog


class TestPygCatalog(TestCase):

    def setUp(self):
        self.conn = psycopg2.connect('')
        self.cat = PygCatalog(self.conn)

    def test_init(self):
        #
        dog = PygCatalog(None)
        self.assertEqual(dog.tables, None)
        self.assertEqual(len(dog.indexes), 0)

    def test_analyze(self):
        #
        dog = PygCatalog(self.conn)
        res = dog.analyze()

        self.assertEqual(res, None)

    def test_pgversion(self):
        #
        version = self.cat.pgversion()

        self.assertTrue(version.startswith('PostgreSQL'))

    def test_schemas(self):
        #
        schemas = self.cat.schemas()
        self.assertEqual(len(schemas), 7)
        self.assertTrue('alice' in schemas)

    def test_default_schema(self):
        # by default look into single schema 'public'
        cat = PygCatalog(self.conn)

        self.assertEqual(cat.default_schemas, ['public'])

    def test_set_default_schema(self):
        # Set a single schema to look in
        cat = PygCatalog(self.conn)

        schema = cat.set_default_schema('alice')
        self.assertEqual(cat.default_schemas, ['alice'])
        self.assertEqual(schema, ['alice'])

    def test_set_default_schemas(self):
        # Set a set of schemas to look in
        # schemas in double are removed
        cat = PygCatalog(self.conn)

        cat.set_default_schemas(['alice', 'public', 'alice'])
        self.assertTrue('alice' in cat.default_schemas)
        self.assertTrue('public' in cat.default_schemas)
        self.assertEqual(len(cat.default_schemas), 2)

    def test_set_default_schema_valuerror(self):
        # Set a single schema to look in
        cat = PygCatalog(self.conn)

        with self.assertRaises(ValueError):
            cat.set_default_schema(['alice'])

        self.assertEqual(cat.default_schemas, ['public'])

    def test_set_default_schemas_valuerror(self):
        # Set a single schema to look in
        cat = PygCatalog(self.conn)

        with self.assertRaises(ValueError):
            cat.set_default_schemas('alice')

        self.assertEqual(cat.default_schemas, ['public'])

    def test_set_default_schemas_valuerror_inside(self):
        # an element in the list is not a string
        cat = PygCatalog(self.conn)

        with self.assertRaises(ValueError):
            cat.set_default_schemas(['alice', 999])

        self.assertEqual(cat.default_schemas, ['alice'])

    # def test_analyze_alice(self):
    #     # Do an ANALYZE on a specific table
    #     dog = PygCatalog(self.conn)
    #     res = dog.analyze('alice.flower')

    #     self.assertEqual(res, None)

    def test_biggest_table(self):
        #
        big = self.cat.biggest_table()
        self.assertEqual(big, ('foo', 'public', 163840, 1000))

    def test_biggest_table_top_ten(self):
        # There is only 2 tables in the database
        big = self.cat.biggest_tables(2)
        self.assertEqual(len(big), 2)

    def test_get_tables(self):
        # tables list from public schema
        conn = psycopg2.connect('')
        cat = PygCatalog(conn)

        cat.get_tables()
        tables = cat.tables
        self.assertEqual(len(tables), 2)
        self.assertEqual(tables['foo'].get('tuple'), 1000)
        self.assertEqual(tables['foo'].get('schema'), 'public')

    def test_get_tables_specific_schema(self):
        # tables list from a specific schema
        conn = psycopg2.connect('')
        cat = PygCatalog(conn)

        tables = cat.get_tables(schema='alice')

        self.assertEqual(len(tables), 1)
        self.assertEqual(tables['flower'].get('tuple'), 1)

    def test_table_tuples(self):
        #
        conn = psycopg2.connect('')
        cat = PygCatalog(conn)

        tuples = cat.table_tuples('foo')
        self.assertEqual(tuples, ('foo', 1000, 'public'))

    def test_table_tuples_specific_schema(self):
        # Number of tuples in a table with a schema specified
        conn = psycopg2.connect('')
        cat = PygCatalog(conn)

        tuples = cat.table_tuples('flower', schema='alice')
        self.assertEqual(tuples, ('flower', 1, 'alice'))


    def test_get_table_columns(self):
        """Read columns from table in the default schema
        """
        cols = self.cat.get_table_columns('foo')

        self.assertEqual(cols, ['id', 'name', 'ratio',
                                'indice', 'created_at'])

    def test_get_table_columns_notexists(self):
        """Read columns from table that does not exists
        """
        cols = self.cat.get_table_columns('this_table_not_exists')

        self.assertIsNone(cols)

    def test_get_table_columns_extended(self):
        """Read columns from table in the default schema
        """
        cols = self.cat.get_table_columns_extended('foo')

        self.assertEqual(len(cols), 5)

    def test_get_operator_class(self):
        #
        cat = PygCatalog(self.conn)

        operators = cat.get_operator_class()
        self.assertTrue(len(operators) > 1)

    def test_get_operator_class_oid(self):
        #
        cat = PygCatalog(self.conn)

        operators = cat.get_operator_class(oid=10010)
        self.assertEqual(len(operators), 1)

    def test_get_triggers(self):
        #
        cat = PygCatalog(self.conn)

        triggers = cat.get_triggers('foo')
        self.assertEqual(len(triggers), 1)
        self.assertEqual(triggers[0]['name'], 'car_insert_trigger')
        self.assertEqual(triggers[0]['event'], 'INSERT')
        self.assertEqual(triggers[0]['timing'], 'BEFORE')

    def test_get_triggers_empty(self):
        # look in table with no triggers
        cat = PygCatalog(self.conn)

        triggers = cat.get_triggers('flower')
        self.assertEqual(triggers, [])

    def test_get_indexes(self):
        #
        cat = PygCatalog(self.conn)

        indexes = cat.get_indexes()
        tables = cat.get_tables()
        self.assertEqual(len(indexes), 2)
        self.assertEqual(tables['foo']['oid'],
                         indexes['foo_name_ratio_idx']['table_oid'])

    def test_get_indexes_schema(self):
        # Get all indexes in a specific schema
        # schema : alice
        cat = PygCatalog(self.conn)

        indexes = cat.get_indexes(schema='alice')
        self.assertEqual(len(indexes), 0)

    def test_get_indexes_table(self):
        # Get all indexes in a specific table
        # schema : alice
        cat = PygCatalog(self.conn)

        indexes = cat.get_indexes(table='foo')
        self.assertEqual(len(indexes), 2)

    def test_is_column_exists(self):
        #
        cat = PygCatalog(self.conn)

        # this column is not indexed
        first = cat.is_column_exists('created_at', 'foo')
        self.assertEqual(first, True)

    def test_column_does_not_exists(self):
        # the column does not exists
        cat = PygCatalog(self.conn)
        second = cat.is_column_exists('missing_column', 'foo')
        self.assertEqual(second, False)

    def test_column_exists_in_another_table(self):
        # the column does not exists in the table but in another one
        cat = PygCatalog(self.conn)
        second = cat.is_column_exists('properties', 'foo')
        self.assertEqual(second, False)

    def test_column_indexed(self):
        # The column is present in one index
        cat = PygCatalog(self.conn)
        first = cat.is_column_indexed('ratio', 'foo')
        self.assertEqual(first, True)

    def test_column_indexed_twice(self):
        # The column is present in two index
        cat = PygCatalog(self.conn)
        first = cat.is_column_indexed('name', 'foo')
        self.assertEqual(first, True)

    def test_column_not_indexed(self):
        # The column is not present in any index
        res = self.cat.is_column_indexed('properties', 'tools')
        self.assertEqual(res, False)

    def test_column_not_indexed_column_dne(self):
        # The column does not exists
        from pygcat import ColumnDoesNotExists

        with self.assertRaises(ColumnDoesNotExists):
            self.cat.is_column_indexed('missing_column', 'tools')

    def test_column_indexed_table_dne(self):
        # The table does not exists
        from pygcat import TableDoesNotExists

        with self.assertRaises(TableDoesNotExists):
            self.cat.is_column_indexed('name', 'missing_table')
