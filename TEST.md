How to test
===========

$ createdb pycatalog
$ export PGDATABASE=pycatalog
$ psql < datas/pygcat_test.sql
$ tox testenv

Coverage
========

$ py.test tests_pygcat.py --cov=pygcat.py --cov-report term-missing
    